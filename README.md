##### acceder à la page web : https://valliermaxime.gitlab.io/projet_metro/

# Représentation des Diagnostics de performance énergétique (DPE) sur l’Agglomération grenobloise

### Téléchargement et installation:

1. **Téléchargez le projet 'deploy_project'** sur votre espace de travail (linux). Ce dossier est composé des éléments suivant:

- **data.csv** -> données de base, diagnostic des logements.
- **commune_grenoble.json** -> localisation des 49 communes
- **nb_bat_commune.xlsx** -> jeux de données contenant le nom de logement par communes
*Ces 3 jeux de données sont les éléments de base du projet*

- **main.ipynb** contient tout le code du projet, transformation et exploitation des données.
- **data_49_communes_column_use.csv** -> CSV de base, ramifier pour garder les données exploitée*
- **Pipfile** -> stockage des dépendance (les packages) qui compose l'application
- **Pipfil.lock** -> liés à l'environnement virtuel qui sera installé votre machine
- **gitlab-ci.yml** -> permet de générer une page web du projet, avec gitlab

2. Une fois le projet sur votre environnement, entrez dans le dossier, clique droit et sélectionnez **ouvrire dans un terminal**. (ou ouvrez simplement le terminal et rendez vous au dossier du projet.)

3. commande: **$ pipenv install** -> créer un environnement virtuel, comme une bulle ou tout les installations des packages se feront sans toucher au paramètres de votre environnement (version des outils entre autres). 

4. commande: **$ pipenv shell** -> Entrez dans cet environnement.

5. commande: **$ jupyter notebook** -> Ouvrez le dossier dans jupyter notebook pour inspecter ou manipuler à votre guise le projet. 

### Explication de l'application:

#### Bref point sur L'outils jupyter

dans le fichier 'main.ipynb', chaque cellule est une entrée, et renvoie une sortie en fonction du code. L'ordre d'exécution des cellules est important. 

Si j'exécute la cellule 2 alors qu'elle dépend en partie du code de la cellule 1, il y aura une erreur. 

Il est donc important d'exécuter les cellules dans le bon ordre.

Pour exécuter toutes les cellules, dans la navbar cliquez sur 'Cellule', puis 'Exécuter tout'.

Pour exécuter une cellule individuellement -> Majuscule + Entrée / VerMaj + Entrée.

Il se peut qu'il y ai un temps d'attente, c'est normal. Si le programme tourne, le cercle vide en haut à droite à côté de 'Python 3' devient plein, et le logo de l'onglet devient un sablier. Lorsque c'est fini, il redevient le cercle initial.

#### Explication du code:

(Plus de détails dans les commentaires du code)

Cellule n1 : 
- Import de toutes les librairies (extension de python)
- création des dataframe nécessaire pour structurer et analyser les données.

Cellule n2: 
- Exécute les 5 premières lignes du dataframe 'df'

Cellule n3:
- Création du dataframe 'nb_bat_com' -> nombre de bâtiments par commune. Ce dataframe contient pour chaques villes les données suivantes:
id, code insee, nom de commune, Année de recensement, nombre de logement.

Cellule n4: 
- Création du dataframe 'dpe_count' -> contient le nombre de logements diagnostiqué par villes.

Cellule n5:
- Création de 'data_ok' -> contient les data de la cellule 3 'nb_bat_com' + la colonne 'nb_logement_controle'

Cellule n6:
- Ajout d'une colonne pour le taux de couverture sur pour chaques villes à 'data_ok'

Cellule n7, ... à , n15 :
- visualisation de données, tout est détaillé dans les commentaires.

##### Déploiment sur une page web statique 'git lab pages'

Le déploiment sur une page web est possible grace à git lab pages. Pour cela, on utilise le fichier présent dans le projet: '.gitlab-ci.yml'. Si ce fichier est push sur votre gitlab avec le reste du contenu du projet, git lab encanche automatiquement le systeme pour créer cette page. Pour acceder au lien de la page, dans le cas ou vous l'installer sur votre environement puis dans votre git lab, voici comment faire:

1. Dans votre git lab, dans la barre d'option sur votre gauche, dérouler le menu jusqu'a 'settings' en bas, puis 'pages'.

2. Cliquez sur le lien au milieu de page, sous : 'Your pages are served under:'